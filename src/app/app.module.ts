import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PerfilComponent } from './perfil/perfil.component';
import { EducationComponent } from './education/education.component';
import { EspecializationsComponent } from './especializations/especializations.component';
import { InformationsPerfilComponent } from './informations-perfil/informations-perfil.component';
import { SkillsComponent } from './skills/skills.component';
import { ContactComponent } from './contact/contact.component';
import { DataCurriculumComponent } from './data-curriculum/data-curriculum.component';
import { InformationsExperienceComponent } from './informations-experience/informations-experience.component';

@NgModule({
  declarations: [
    AppComponent,
    PerfilComponent,
    EducationComponent,
    EspecializationsComponent,
    InformationsPerfilComponent,
    SkillsComponent,
    ContactComponent,
    DataCurriculumComponent,
    InformationsExperienceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
