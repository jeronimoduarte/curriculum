import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspecializationsComponent } from './especializations.component';

describe('EspecializationsComponent', () => {
  let component: EspecializationsComponent;
  let fixture: ComponentFixture<EspecializationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspecializationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspecializationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
