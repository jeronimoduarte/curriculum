import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationsExperienceComponent } from './informations-experience.component';

describe('InformationsExperienceComponent', () => {
  let component: InformationsExperienceComponent;
  let fixture: ComponentFixture<InformationsExperienceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationsExperienceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationsExperienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
