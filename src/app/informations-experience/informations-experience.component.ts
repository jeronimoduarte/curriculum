import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-informations-experience',
  templateUrl: './informations-experience.component.html',
  styleUrls: ['./informations-experience.component.less']
})
export class InformationsExperienceComponent implements OnInit {

  constructor() { }
  @Input() description: String
  @Input() title: String
  @Input() cursos: boolean
  @Input() especializations: boolean
  ngOnInit() {
  }

}
