import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-informations-perfil',
  templateUrl: './informations-perfil.component.html',
  styleUrls: ['./informations-perfil.component.less']
})
export class InformationsPerfilComponent implements OnInit {

  constructor() { }

  @Input() description:any
  @Input() title:String
  @Input() isContact: boolean
  @Input() isSkills:boolean
  ngOnInit() {
  }

}
