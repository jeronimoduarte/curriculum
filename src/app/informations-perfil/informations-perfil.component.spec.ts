import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationsPerfilComponent } from './informations-perfil.component';

describe('InformationsPerfilComponent', () => {
  let component: InformationsPerfilComponent;
  let fixture: ComponentFixture<InformationsPerfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformationsPerfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationsPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
