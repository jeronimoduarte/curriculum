import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataCurriculumComponent } from './data-curriculum.component';

describe('DataCurriculumComponent', () => {
  let component: DataCurriculumComponent;
  let fixture: ComponentFixture<DataCurriculumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataCurriculumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataCurriculumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
