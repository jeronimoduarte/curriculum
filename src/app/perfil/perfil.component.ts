import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.less']
})
export class PerfilComponent implements OnInit {

  constructor() { }
  description: String
  experience:boolean
  formation:boolean
  especialization:boolean

  ngOnInit() {
    this.description = 'Profissional com 7 anos de experiência em desenvolvimento de sistemas para web. Cursando sistemas para internet, terceiro semestre, formação técnica em web designer pelo Senac e profissionalizante em designer gráfico por Eleven C.  Conhecimento em diversas áreas da tecnologia, ambição em aprendizado e boa comunicação.';
    this.experience = true
  }

  showTab = (tab) => {
    this.experience = tab === 'experience' ? true : false;
    this.formation = tab === 'formation' ? true : false
    this.especialization = tab === 'especialization' ? true : false
  }


}
